#![feature(proc_macro_hygiene, decl_macro)]

use serde::{Deserialize, Serialize};
use std::sync::Mutex;

#[macro_use]
extern crate rocket;

use rocket::State;
use rocket_contrib::json;
use rocket_contrib::json::{Json, JsonValue};

#[get("/")]
fn greet() -> &'static str {
    "Chuck Norris counted to infinity - twice!"
}

#[get("/<id>", format = "json")]
fn get_person(id: usize, state: State<Mutex<MemoryState>>) -> Option<Json<Person>> {
    let state = state.lock().unwrap();
    state.persons.get(id).map(|p| Json(p.clone()))
}
#[get("/", format = "json")]
fn get_persons(state: State<Mutex<MemoryState>>) -> Json<Vec<Person>> {
    let state = state.lock().unwrap();
    Json(state.persons.clone())
}

#[post("/", format = "json", data = "<person>")]
fn post_person(person: Json<Person>, state: State<Mutex<MemoryState>>) -> JsonValue {
    state.lock().expect("state lock.").persons.push(person.0);
    json!({ "status": "ok" })
}

fn main() {
    rocket::ignite()
        .mount("/person", routes![get_person, get_persons, post_person])
        .mount("/", routes![greet])
        .manage(Mutex::new(MemoryState::new()))
        .launch();
}

#[derive(Clone)]
struct MemoryState {
    persons: Vec<Person>,
}

#[derive(Serialize, Deserialize, Clone)]
struct Person {
    first: String,
    last: String,
}

impl MemoryState {
    fn new() -> Self {
        Self {
            persons: vec![
                Person {
                    first: String::from("Chuck"),
                    last: String::from("Norris"),
                },
                Person {
                    first: String::from("Gunnery Sgt."),
                    last: String::from("Hartmann"),
                },
            ],
        }
    }
}
