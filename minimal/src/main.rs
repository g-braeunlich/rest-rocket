#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

#[get("/")]
fn greet() -> &'static str {
    "Chuck Norris counted to infinity - twice!"
}

fn main() {
    rocket::ignite().mount("/", routes![greet]).launch();
}
