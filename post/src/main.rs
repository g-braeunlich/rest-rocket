#![feature(proc_macro_hygiene, decl_macro)]

use serde::Deserialize;

#[macro_use]
extern crate rocket;

use rocket_contrib::json;
use rocket_contrib::json::{Json, JsonValue};

#[post("/", format = "json", data = "<person>")]
fn post_person(person: Json<Person>) -> JsonValue {
    json!({
        "status": "ok",
        "msg": format!("Hello {} {}", person.first, person.last)
    })
}

fn main() {
    rocket::ignite().mount("/", routes![post_person]).launch();
}

#[derive(Deserialize)]
struct Person {
    first: String,
    last: String,
}
