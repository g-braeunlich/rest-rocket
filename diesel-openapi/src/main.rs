#![feature(proc_macro_hygiene, decl_macro)]

pub mod models;
pub mod schema;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate diesel;

use diesel::prelude::*;

use rocket_contrib::json::{Json, JsonValue};

use rocket_okapi::swagger_ui::{make_swagger_ui, SwaggerUIConfig};
use rocket_okapi::{openapi, routes_with_openapi};

use models::{NewPerson, Person};
use schema::persons;
use schema::persons::dsl::persons as all_persons;

#[database("sqlite_db")]
struct DbConn(diesel::SqliteConnection);

#[openapi]
#[get("/")]
fn greet() -> &'static str {
    "Chuck Norris counted to infinity - twice!"
}

#[openapi]
#[get("/<id>", format = "json")]
fn get_person(id: i32, conn: DbConn) -> Option<Json<Person>> {
    all_persons
        .find(id)
        .get_result::<Person>(&conn.0)
        .ok()
        .map(|p| Json(p.clone()))
}

#[openapi]
#[get("/", format = "json")]
fn get_persons(conn: DbConn) -> Json<Vec<Person>> {
    let results = all_persons
        .order(persons::id.desc())
        .load::<Person>(&conn.0)
        .expect("Error loading persons");
    Json(results)
}

#[openapi]
#[post("/", format = "json", data = "<person>")]
fn post_person(person: Json<NewPerson>, conn: DbConn) -> JsonValue {
    diesel::insert_into(persons::table)
        .values(person.0)
        .execute(&conn.0)
        .expect("Error saving new person");
    json!({ "status": "ok" })
}

fn get_docs() -> SwaggerUIConfig {
    SwaggerUIConfig {
        url: "/person/openapi.json".to_string(),
        ..Default::default()
    }
}

fn main() {
    rocket::ignite()
        .mount(
            "/person",
            routes_with_openapi![get_person, get_persons, post_person],
        )
        .mount("/", routes_with_openapi![greet])
        .mount("/swagger", make_swagger_ui(&get_docs()))
        .attach(DbConn::fairing())
        .launch();
}
