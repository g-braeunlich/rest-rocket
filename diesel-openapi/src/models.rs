use super::schema::persons;
use rocket_okapi::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Queryable, JsonSchema)]
pub struct Person {
    pub id: i32,
    pub first: String,
    pub last: String,
}

#[derive(Insertable, Deserialize, JsonSchema)]
#[table_name = "persons"]
pub struct NewPerson {
    pub first: String,
    pub last: String,
}
