CREATE TABLE IF NOT EXISTS persons (
  id INTEGER PRIMARY KEY NOT NULL,
  first VARCHAR NOT NULL,
  last VARCHAR NOT NULL
);

INSERT INTO persons (first, last) VALUES ('Chuck', 'Norris');
INSERT INTO persons (first, last) VALUES ('Gunnery Sgt.', 'Hartmann');
