# REST with rocket demo

Dependency: rust nigthly

This is a demo of simple REST services:

* *minimal*: only `GET /`
* *post*: only `POST /`
* *rest*:
    - `GET /`
    - `GET /person/`
    - `GET /person/<id>`
    - `POST /person/`
* *diesel*: same as *rest* but uses a sqlite backend instead of an in
  memory state
* *diesel-openapi*: same as *diesel*, also includes an autogenerated
  `/swagger` endpoint

## Build

The following builds all examples (`--release` for an optimized build
without debug info):

```bash
cargo build [ --release ]
```

## Run specific example
```bash
cargo run --bin <binary>
```
where `<binary>` is one of `rest-rocket-minimal`, `rest-rocket-post`,
`rest-rocket`.

To run the diesel examples (they need the sqlite files):

```bash
cd <dir>
cargo run
```
where `<dir>` is `diesel` or `diesel-openapi`.


## Run all examples (presentation mode)

This requires [warp-cors](https://github.com/bassetts/warp-cors), a
CORS proxy
(actually only for the python file server, it seems that rocket
supports CORS out of the box).
Install it with

```bash
cargo install warp-cors
```

Then run:

```bash
./start.sh
```

This runs all examples in the background and redirects their output to
files specified via an environment variable `$LOG_DIR` (default
`/tmp/`).
Also a python file server is started, serving the logs.
