#![feature(proc_macro_hygiene, decl_macro)]

pub mod models;
pub mod schema;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate diesel;

use diesel::prelude::*;

use rocket_contrib::json::{Json, JsonValue};

use models::{NewPerson, Person};
use schema::persons;
use schema::persons::dsl::persons as all_persons;

#[database("sqlite_db")]
struct DbConn(diesel::SqliteConnection);

#[get("/")]
fn greet() -> &'static str {
    "Chuck Norris counted to infinity - twice!"
}

#[get("/<id>", format = "json")]
fn get_person(id: i32, conn: DbConn) -> Option<Json<Person>> {
    all_persons
        .find(id)
        .get_result::<Person>(&conn.0)
        .ok()
        .map(|p| Json(p.clone()))
}

#[get("/", format = "json")]
fn get_persons(conn: DbConn) -> Json<Vec<Person>> {
    let results = all_persons
        .order(persons::id.desc())
        .load::<Person>(&conn.0)
        .expect("Error loading persons");
    Json(results)
}

#[post("/", format = "json", data = "<person>")]
fn post_person(person: Json<NewPerson>, conn: DbConn) -> JsonValue {
    diesel::insert_into(persons::table)
        .values(person.0)
        .execute(&conn.0)
        .expect("Error saving new person");
    json!({ "status": "ok" })
}

fn main() {
    rocket::ignite()
        .mount("/person", routes![get_person, get_persons, post_person])
        .mount("/", routes![greet])
        .attach(DbConn::fairing())
        .launch();
}
