table! {
    persons (id) {
        id -> Integer,
        first -> Text,
        last -> Text,
    }
}
