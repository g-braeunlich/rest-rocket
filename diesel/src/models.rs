use super::schema::persons;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Queryable)]
pub struct Person {
    pub id: i32,
    pub first: String,
    pub last: String,
}

#[derive(Insertable, Deserialize)]
#[table_name = "persons"]
pub struct NewPerson {
    pub first: String,
    pub last: String,
}
