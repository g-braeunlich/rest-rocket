LOG_DIR=${LOG_DIR:-/tmp}
pids=()

function shutdown() {
    echo "terminating ${pids[@]}"
    for pid in "${pids[@]}" ; do
        kill "$pid"
    done
}

trap shutdown SIGINT

# minimal
ROCKET_PORT=8001 ~/.cargo/bin/cargo run --bin rest-rocket-minimal \
           > $LOG_DIR/rest-rocket-minimal.log &
pids+=( "$!" ) 

# post
ROCKET_PORT=8002 ~/.cargo/bin/cargo run --bin rest-rocket-post \
           > $LOG_DIR/rest-rocket-post.log &
pids+=( "$!" )

# rest full
ROCKET_PORT=8003 ~/.cargo/bin/cargo run --bin rest-rocket \
           > $LOG_DIR/rest-rocket.log &
pids+=( "$!" )

# diesel openapi
pushd diesel-openapi > /dev/null
ROCKET_PORT=8004 ~/.cargo/bin/cargo run \
           > $LOG_DIR/rest-rocket-diesel-openapi.log &
pids+=( "$!" )
popd > /dev/null

# webserver
python -m http.server -d "$LOG_DIR" 8100 &
pids+=( "$!" )

# warp cors
~/.cargo/bin/warp-cors
